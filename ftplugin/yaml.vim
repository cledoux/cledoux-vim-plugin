" Size 2 tabs just works so much better with yaml.
set shiftwidth=2
set tabstop=2
" Ensure autoindent is on.
set autoindent
" Always use unix fileformat
set fileformat=unix
" Ensure use of spaces instead of tabs.
" YAML doesn't like tabs
set expandtab

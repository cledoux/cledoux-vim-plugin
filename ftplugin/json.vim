" Compact display
set shiftwidth=2
set tabstop=2
" Ensure autoindent is on.
set autoindent
" Always use unix fileformat
set fileformat=unix
" Ensure use of spaces instead of tabs.
set expandtab
" Fold interal structures
set foldmethod=syntax

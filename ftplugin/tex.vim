" Modeline and Notes {{{ 
" vim: set foldmarker={{{,}}} foldmethod=marker foldlevel=0 spell: 
"
"   Author: Charles LeDoux
"
"   This file is in the public domain.
"
" }}}

" Include : character in keywords
set iskeyword+=:
" Turn on spell checking by default
set spell
" Do not break already too long lines
set fo+=l

" Smart Quotes {{{
" Function for smart-quotes: double
" http://tex.stackexchange.com/a/8542/3325
function! s:TexQuotes()
    if getline('.')[0:col(".")] =~ '\(^\|[^\\]\)%'
       let kinsert = "\""
    else
        let kinsert = "\'\'"
        let left = getline('.')[col('.')-2]
        if left =~ '^\(\|\s\|{\|(\|\[\|&\)$'
            let kinsert = "\`\`"
        elseif left == "\\"
            let kinsert = "\""
        endif
    endif
    return kinsert
endfunction
" mapping for quotation marks
inoremap <buffer> " <C-R>=<SID>TexQuotes()<CR>
" Function for smart-quotes: single
function! s:TexSingQuotes()
    if getline('.')[0:col(".")] =~ '\(^\|[^\\]\)%'
       let schminsert = "'"
    else
        let schminsert = "'"
        let left = getline('.')[col('.')-2]
        if left =~ '^\(\|\s\|{\|(\|\[\|&\)$'
            let schminsert = '`'
        endif
    endif
    return schminsert
endfunction
" mapping for single quotation mark
inoremap <buffer> ' <C-R>=<SID>TexSingQuotes()<CR>
" }}}

" One sentence per line formatting {{{

" Disabled because I can't find a solution for forcing a softwrap
" at 80 columns. A smarter formatexpr might be useful here. 

" " Set the format function.
" " http://vi.stackexchange.com/a/2848/2226
" function! MyFormatExpr(start, end)
"     silent execute a:start.','.a:end.'s/[.!?]\zs /\r/g'
" endfunction
"
" set formatexpr=MyFormatExpr(v:lnum,v:lnum+v:count-1)
"
" " Use soft wrapping with indicator for long lines
" " http://stackoverflow.com/a/5931919/558820
" set wrap linebreak
" let &showbreak='\u21aa '
"
" " Keep expected behavior of movement keys
" noremap j gj
" noremap k gk
" noremap gj j
" noremap gk k

" }}}

set spell
set formatoptions+=l

" Use Markdown tables
let g:table_mode_corner='|'
let g:table_mode_corner_corner='|'
let g:table_mode_header_fillchar='-'


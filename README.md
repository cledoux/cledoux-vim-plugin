# Charles LeDoux custom VIM plugin.

This plugin sets many of my preferred defaults.

It does *not* contain my .vimrc.

The only reason this plugin exists is because I have a number of
options (mostly ftplugins) that should not or can not be placed directly
in the vimrc. 